from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
import threading
import redis
import time
def thfunc(stopper):
	i=1
	while(True):
		if (stopper.is_set()):
			break
		r = redis.StrictRedis(host='localhost', port=6379, db=0)
		r.publish('test',i)
		time.sleep(2)
		i=i+1
	

# Create your views here.
stopper=None
@csrf_exempt
def home(request):
	stopper=threading.Event()
	thread = threading.Thread(target=thfunc, args=(stopper,))
	thread.start()
	return render(request,'cl/home.html')

def threadbreaker(request):
	stopper.set()
	return HttpResponse("thread killed!!!")
